﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.ViewModel.OKR
{
  public class TreeViewRenameOKR
    {
        public int key { get; set; }
        public string code { get; set; }
        public string title { get; set; }
        public string link { get; set; }
        public string path { get; set; }
        public int levelnumber { get; set; }
        public int parentid { get; set; }
    }
}
