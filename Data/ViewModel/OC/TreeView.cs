﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Data.ViewModel.OC
{
    public class TreeView
    {
        public TreeView()
        {
            this.children = new List<TreeView>();
            CreatedDate = DateTime.Now;
        }


        public int key { get; set; }
        public string title { get; set; }
        public string code { get; set; }
        public double levelnumber { get; set; }
        public int parentid { get; set; }
        public string link { get; set; }
        public string linkpath { get; set; }
        public bool state { get; set; }
        public string index { get; set; }
        public DateTime CreatedDate { get; set; }
        public bool HasChildren
        {
            get { return children.Any(); }
        }

        public List<TreeView> children { get; set; }
    }
}
