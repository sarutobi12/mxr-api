﻿using Data.Models;
using Microsoft.EntityFrameworkCore;
using System;

namespace Data
{
    public class DataContext : DbContext
    {
        public DbSet<Role> Roles { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Machine> machine { get; set; }
        public DbSet<Rawdata> rawdata { get; set; }
        public DbSet<Setting> setting { get; set; }

        public DbSet<Location> location { get; set; }


        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {

        }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Manager>().HasKey(ba => new { ba.UserID, ba.ProjectID });

            //builder.Entity<TeamMember>().HasKey(ba => new { ba.UserID, ba.ProjectID });
            //builder.Entity<Tag>().HasKey(ba => new { ba.TaskID, ba.UserID });
            builder.Entity<OCUser>().HasKey(ba => new { ba.UserID, ba.OCID });

        }
    }
}
