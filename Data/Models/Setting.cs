﻿using Data.Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Data.Models
{
   public class Setting
    {

       
        public string id { get; set; }
        public int standardRPM { get; set; }
        public int minRPM { get; set; }
        public int startBuzzerAfter { get; set; }
        public int stopwatch { get; set; }
        public int timer { get; set; }
  

    }
}
