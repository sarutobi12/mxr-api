﻿using Data.Interface;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Models
{
   public class Role : IEntity
    {
        public int id{ get; set; }
        public string Name { get; set; }
    }
}
