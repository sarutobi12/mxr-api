﻿using Data.Interface;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Models
{
   public class TeamMember : IEntity
    {

        public int id { get; set; }
        public string Name { get; set; }
        public string JobName { get; set; }
        public string Image { get; set; }
        public int Position { get; set; }
        public string FacebookURL { get; set; }
        public string TwitterURL { get; set; }
        public string InstagramURL { get; set; }
        
    }
}
