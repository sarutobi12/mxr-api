﻿using Data.Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Data.Models
{
   public class Rawdata
    {


        public int id { get; set; }

        public string machineID { get; set; }
        public int RPM { get; set; }
        public DateTime createddatetime { get; set; }
        public int duration { get; set; }

        public int sequence { get; set; }


    }
}
