﻿using Data.Interface;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Models
{
   public class Machine: IEntity
    {

        //public Project()
        //{
        //    CreateTime = DateTime.Now;
        //}

        public int id { get; set; }
        //public int ID { get; set; }
        public string machineID { get; set; }
        public string description { get; set; }
        public int locationID { get; set; }
        //public string WorkBy { get; set; }
        //public DateTime CreateTime { get; set; }
        //public virtual List<Manager> Managers { get; set; }
        //public virtual List<TeamMember> TeamMembers { get; set; }

    }
}
