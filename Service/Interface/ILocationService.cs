﻿using Data.Models;
using Data.ViewModel.Project;
using Data.ViewModel.Slide;
using Service.Helpers;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Service.Interface
{
    public interface ILocationService
    {
     
        Location Get(int id);

        bool Add(Location model);
        bool Update(Location model);
        bool Delete(int id);
    }
}
