﻿using Data.Models;
using Data.ViewModel.Project;
using Data.ViewModel.Slide;
using Service.Helpers;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Service.Interface
{
    public interface IRawService
    {
        //Task<bool> Create(Slide entity);
        //Task<bool> Update(Slide entity);
        //Task<bool> Delete(int id);
        //Task<Slide> GetByID(int id);
        Task<List<Machine>> GetAll();
        object GetDetail(string id, string start = "", string end = "");
        object GetDetail1(string id);
        Task<object> GetRPM(string id);
        object ExportExcel(string id, string startDate, string endDate);
        object Test(string id);
        object Test2(string id, string startDate, string endDate);
    }
}
