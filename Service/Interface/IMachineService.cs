﻿using Data.Models;
using Data.ViewModel.Project;
using Data.ViewModel.Slide;
using Service.Helpers;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Service.Interface
{
    public interface IMachineService
    {
        //Task<bool> Create(Slide entity);
        //Task<bool> Update(Slide entity);
        //Task<bool> Delete(int id);
        //Task<Slide> GetByID(int id);
        Task<List<Machine>> GetAll();
        object Get(string id);
        object Add(Machine model);
        bool Update(Machine model);

        bool Delete(string id);
    }
}
