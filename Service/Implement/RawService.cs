﻿using AutoMapper;
using Data;
using Data.Models;
using Data.ViewModel;
using Data.ViewModel.Slide;
using Microsoft.EntityFrameworkCore;
using OfficeOpenXml;
using OfficeOpenXml.Table;
using Service.Helpers;
using Service.Interface;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Implement
{
    public class RawService : IRawService
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;

        //public static IWebHostEnvironment _environment;
        public RawService(DataContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
            //_environment = environment;
        }

        //public async Task<bool> Create(Slide entity)
        //{
        //    await _context.Slides.AddAsync(entity);

        //    try
        //    {
        //        await _context.SaveChangesAsync();
        //        return true;
        //    }
        //    catch (Exception)
        //    {
        //        return false;

        //    }


        //}

        //public async Task<bool> Delete(int id)
        //{
        //    var entity = await _context.Slides.FindAsync(id);
        //    if (entity == null)
        //    {
        //        return false;
        //    }

        //     _context.Slides.Remove(entity);
        //    try
        //    {
        //        await _context.SaveChangesAsync();
        //        return true;
        //    }
        //    catch (Exception)
        //    {
        //        return false;

        //    }
        //}

        public async Task<List<Machine>> GetAll()
        {
            return await _context.machine.ToListAsync();
        }


        public object GetDetail(string id, string startDate, string endDate)
        {
            var locationID = _context.machine.Where(x => x.machineID.Equals(id)).FirstOrDefault().locationID;
            return new
            {

                model = GetMachineDetail(id, startDate, endDate),
                standard = _context.setting.Where(x => x.id.Equals(id)).FirstOrDefault().standardRPM,
                minstandard = _context.setting.Where(x => x.id.Equals(id)).FirstOrDefault().minRPM,
                location = _context.location.Where(x => x.id.Equals(locationID)).FirstOrDefault().locationname,
            };
        }
        public object GetDetail1(string id)
        {
            id = id.ToUpper();
            var model = new List<MachineDetail>();
            var sequenceList = _context.rawdata.Where(x => x.machineID == id).Select(y => y.sequence).Distinct().ToList();

            foreach (var item in sequenceList)
            {
                var vm = new MachineDetail();
                vm.MachineID = _context.rawdata.Where(x => x.machineID.Equals(id) && x.sequence == item).Select(y => y.machineID).FirstOrDefault();
                vm.StartTime = _context.rawdata.Where(x => x.machineID.Equals(id) && x.sequence == item).Select(y => y.createddatetime).Min().ToString("dddd, dd MMMM yyyy hh:mm tt");
                vm.EndTime = _context.rawdata.Where(x => x.machineID.Equals(id) && x.sequence == item).Select(y => y.createddatetime).Max().ToString("dddd, dd MMMM yyyy hh:mm tt");
                vm.RPM = _context.rawdata.Where(x => x.machineID.Equals(id) && x.sequence == item).Select(y => y.RPM).Average();
                model.Add(vm);
            }
            model = model.ToArray().Reverse().Take(15).ToList();
            return new
            {
                model,
                standard = _context.setting.Where(x => x.id.Equals(id)).FirstOrDefault()?.standardRPM
            };
        }
        public List<MachineDetail> GetMachineDetail(string id, string startDate, string endDate)
        {
            var format = "hh:mm:ss tt";
            var formatDate = "dddd, dd MMMM yyyy";
            var start = new DateTime();
            var end = new DateTime();
            var model = new List<MachineDetail>();
            var standard = _context.setting.Where(x => x.id.Equals(id))
                                                    .FirstOrDefault().standardRPM;
            var sequenceList = _context.rawdata.Where(x => x.machineID == id)
                .Select(y => new
                {
                    y.createddatetime,
                    y.sequence
                })
                .ToList();
            if (!startDate.IsNullOrEmpty() && !endDate.IsNullOrEmpty())
            {
                start = Convert.ToDateTime(startDate);
                end = Convert.ToDateTime(endDate);
                sequenceList = sequenceList.Where(x => x.createddatetime.Date >= start.Date && x.createddatetime.Date <= end.Date).ToList();
            }
            else if (!startDate.IsNullOrEmpty())
            {
                start = Convert.ToDateTime(startDate);

                sequenceList = sequenceList.Where(x => x.createddatetime.Date >= start.Date).ToList();
            }
            else if (!endDate.IsNullOrEmpty())
            {
                end = Convert.ToDateTime(endDate);

                sequenceList = sequenceList.Where(x => x.createddatetime.Date <= end.Date).ToList();
            }
            else
            {
                sequenceList = sequenceList.Where(x => x.createddatetime.Date == DateTime.Now.Date).ToList();
            };
            int count = 1;
            foreach (var item in sequenceList.Select(x => x.sequence).Distinct())
            {

                var max = _context.rawdata.Where(x => x.machineID.Equals(id) && x.sequence == item).Select(y => y.createddatetime).Max();
                var min = _context.rawdata.Where(x => x.machineID.Equals(id) && x.sequence == item).Select(y => y.createddatetime).Min();
                var totalMinutes = Math.Round(TimeSpan.FromTicks(max.Ticks - min.Ticks).TotalMinutes, 2);
                var totalSeconds = TimeSpan.FromTicks(max.Ticks - min.Ticks).TotalSeconds;
                var vm = new MachineDetail();
                vm.No = count;
                vm.MachineID = _context.rawdata.Where(x => x.machineID.Equals(id) && x.sequence == item).Select(y => y.machineID).FirstOrDefault();
                vm.StartTime = min.ToString(format);
                vm.EndTime = max.ToString(format);
                vm.RPM = _context.rawdata.Where(x => x.machineID.Equals(id) && x.sequence == item).Select(y => y.RPM).Average();
                vm.Date = _context.rawdata.Where(x => x.machineID.Equals(id) && x.sequence == item).Select(y => y.createddatetime).Max().ToString(formatDate);
                vm.Minutes = totalMinutes;
                vm.Second = (int)totalSeconds;
                vm.Status = standard < _context.rawdata.Where(x => x.machineID.Equals(id) && x.sequence == item).Select(y => y.RPM).Average();
                model.Add(vm);
                count++;
            }

            model = model.ToArray().Reverse().Take(30).ToList();
            return model;
        }

        public async Task<object> GetRPM(string id)
        {

            var model = await _context.rawdata.OrderByDescending(x => x.createddatetime).FirstOrDefaultAsync(x => x.machineID == id);

            return new
            {
                model.RPM,
                display = true
            };

        }

        public object ExportExcel(string id, string startDate, string endDate)
        {
            DataTable Dt = new DataTable();
            Dt.Columns.Add("MachineID", typeof(string));
            Dt.Columns.Add("Date", typeof(string));
            Dt.Columns.Add("Start Time", typeof(string));
            Dt.Columns.Add("End Time", typeof(string));
            Dt.Columns.Add("RPM", typeof(double));
            Dt.Columns.Add("Minutes", typeof(int));
            var model = GetMachineDetail(id, startDate, endDate);
            foreach (var item in model)
            {
                Dt.Rows.Add(item.MachineID, item.Date, item.StartTime, item.EndTime, item.RPM, item.Minutes);

            }
            var memoryStream = new MemoryStream();
            using (var excelPackage = new ExcelPackage(memoryStream))
            {
                var worksheet = excelPackage.Workbook.Worksheets.Add("Sheet1");
                worksheet.Cells["A1"].LoadFromDataTable(Dt, true, TableStyles.None);
                worksheet.Cells["A1:AN1"].Style.Font.Bold = true;
                worksheet.DefaultRowHeight = 18;

                worksheet.Column(2).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                worksheet.Column(6).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Column(7).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.DefaultColWidth = 20;
                worksheet.Column(2).AutoFit();

                return excelPackage.GetAsByteArray();
            }
        }

        public object Test(string id)
        {

            var model = _context.rawdata
                .Where(x => x.machineID == id)
                .OrderByDescending(x => x.createddatetime)
                .Select(x => x.RPM)
                .Take(30)
                .ToArray().Reverse();

            return model;

        }

        public object Test2(string id, string startDate, string endDate)
        {

            var start = new DateTime();

            var end = new DateTime();
            start = Convert.ToDateTime(startDate);
            end = Convert.ToDateTime(endDate);
            var source = _context.rawdata.Where(x => x.machineID == id && x.createddatetime.Date >= start.Date && x.createddatetime.Date <= end.Date).ToList();
            var aa = new List<Class1>();
            var acs = source.Select(x => x.sequence).Distinct();
            foreach (var item in acs)
            {
                var a = new Class1();
                a.Key = item.ToString();
                var b = new List<Rawdata>();
                foreach (var item2 in source)
                {
                    if (item == item2.sequence)
                        b.Add(item2);
                }
                a.Values = b;
                aa.Add(a);
            }

            var rpm = new List<string>();
            var rpm2 = new List<Class2>();
            var time = new List<string>();
            var s2 = DateTime.MinValue;
            var s1 = string.Empty;
            var s1tamp = string.Empty;
            var count = 1;
            foreach (var item in aa)
            {
                var last = new List<string>();
                foreach (var items in item.Values)
                {
                    rpm2.Add(new Class2 { createddatetime = items.createddatetime, RPM = items.RPM });
                    rpm.Add(items.RPM.ToString());
                    time.Add(items.createddatetime.ToString());
                }

                // count == 1
                if(count == 1)
                {
                    // lay gia tri cuoi cua sequence
                    s1 = item.Values.Select(x => x.createddatetime.ToString()).LastOrDefault();
                    // gan gia tri cuoi cua sequence vao bien tam
                    s1tamp = item.Values.Select(x => x.createddatetime.ToString()).LastOrDefault();

                }

                // neu count = 1 , bo qua buoc nay
                // neu count > 1
                if (count > 1)
                {
                    s2 = item.Values.Select(x => x.createddatetime).FirstOrDefault();
                    s1tamp = item.Values.Select(x => x.createddatetime.ToString()).LastOrDefault();
                }

                if (s2 != DateTime.MinValue)
                {
                    var timeTemp = Convert.ToDateTime(s1);
                    var dd = timeTemp;
                    if (s2.CompareTo(timeTemp) != 0)
                    {
                        TimeSpan MySpan = new TimeSpan(timeTemp.Hour, timeTemp.Minute, timeTemp.Second);
                        var a = MySpan.TotalSeconds;
                        TimeSpan MySpan2 = new TimeSpan(s2.Hour, s2.Minute, s2.Second);
                        var b = MySpan2.TotalSeconds -1;
                        for (double i = a; i < b; i ++)
                        {
                            var t = dd.AddSeconds(1);
                            dd = t;
                            var rpmTemp = 0;
                            rpm.Add(rpmTemp.ToString());
                            time.Add(t.ToString("yyyy/MM/DD hh:mm:ss tt"));
                            rpm2.Add(new Class2 { createddatetime = t, RPM = 0 });
                        }
                    }
                }

                s2 = DateTime.MinValue;
                s1 = s1tamp;
                count++;

            }
            return new
            {
                model2=  rpm2.OrderBy(x => x.createddatetime).ToList()
            };
            //var model2 = new List<Model>();
            //var start = new DateTime();

            //var end = new DateTime();
            //var model = _context.rawdata.Where(x => x.machineID == id)
            //.OrderByDescending(x => x.createddatetime)
            //.Select(x => x.RPM)
            //.ToArray();
            //var time = _context.rawdata.Where(x => x.machineID == id)
            //.OrderByDescending(x => x.createddatetime)
            //.Select(x => x.createddatetime)
            //.ToArray();
            //if (!startDate.IsNullOrEmpty() && !endDate.IsNullOrEmpty())
            //{
            //    var format = "hh:mm:ss";
            //    start = Convert.ToDateTime(startDate);
            //    end = Convert.ToDateTime(endDate);
            //    var modelarr = _context.rawdata.Where(x => x.createddatetime.Date >= start.Date && x.createddatetime.Date <= end.Date).Select(x => x.RPM.ToString()).ToList();
            //    var timearr = _context.rawdata.Where(x => x.createddatetime.Date == DateTime.Now.Date).Select(x => x.createddatetime.ToString(format)).ToList();
            //    model2 = _context.rawdata
            //            .Where(x => x.createddatetime.Date >= start.Date && x.createddatetime.Date <= end.Date && x.machineID == id)
            //            .OrderBy(x => x.createddatetime)
            //            .Select(x => new Model
            //            {
            //                MachineID = x.machineID,
            //                RPM = x.RPM ,
            //                createddatetime = x.createddatetime.ToString(),
            //            }).ToList();

            //}
            //else if (!startDate.IsNullOrEmpty())
            //{
            //    start = Convert.ToDateTime(startDate);

            //    model = _context.rawdata.Where(x => x.createddatetime.Date >= start.Date && x.createddatetime.Date <= end.Date).Select(x => x.RPM).ToArray();
            //    time = _context.rawdata.Where(x => x.createddatetime.Date == DateTime.Now.Date).Select(x => x.createddatetime).ToArray();

            //}
            //else if (!endDate.IsNullOrEmpty())
            //{
            //    end = Convert.ToDateTime(endDate);

            //    model = _context.rawdata.Where(x => x.createddatetime.Date >= start.Date && x.createddatetime.Date <= end.Date).Select(x => x.RPM).ToArray();
            //    time = _context.rawdata.Where(x => x.createddatetime.Date == DateTime.Now.Date).Select(x => x.createddatetime).ToArray();

            //}
            //else
            //{
            //    model = _context.rawdata.Where(x => x.createddatetime.Date == DateTime.Now.Date).Select(x => x.RPM).ToArray();
            //    time = _context.rawdata.Where(x => x.createddatetime.Date == DateTime.Now.Date).Select(x => x.createddatetime).ToArray();

            //};

            //return new
            //{
            //    a
            //};

        }



    }
}
