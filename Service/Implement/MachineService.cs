﻿using AutoMapper;
using Data;
using Data.Models;
using Data.ViewModel.Slide;
using Microsoft.EntityFrameworkCore;
using OfficeOpenXml;
using OfficeOpenXml.Table;
using Service.Helpers;
using Service.Interface;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Implement
{
    public class MachineService : IMachineService
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;

        //public static IWebHostEnvironment _environment;
        public MachineService(DataContext context , IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
            //_environment = environment;
        }

        //public async Task<bool> Create(Slide entity)
        //{
        //    await _context.Slides.AddAsync(entity);

        //    try
        //    {
        //        await _context.SaveChangesAsync();
        //        return true;
        //    }
        //    catch (Exception)
        //    {
        //        return false;

        //    }


        //}

        //public async Task<bool> Delete(int id)
        //{
        //    var entity = await _context.Slides.FindAsync(id);
        //    if (entity == null)
        //    {
        //        return false;
        //    }

        //     _context.Slides.Remove(entity);
        //    try
        //    {
        //        await _context.SaveChangesAsync();
        //        return true;
        //    }
        //    catch (Exception)
        //    {
        //        return false;

        //    }
        //}
        public async Task<List<Machine>> GetAll()
        {
            return await _context.machine.ToListAsync();
        }

        public object Get(string id)
        {
            var result = new Machine();
            try
            {
                result = _context.machine.FirstOrDefault(x => x.machineID == id);
            }
            catch (System.Exception)
            {

            }
            return new
            {
                data = result,
                location = _context.location
            };

        }


        public object Add(Machine model)
        {
            try
            {
                _context.Add(model);
                _context.SaveChanges();
            }
            catch (System.Exception)
            {

                return new { status = false };
            }
            return new { status = true, locations = _context.location };

        }

        public bool Update(Machine model)
        {
            try
            {
                var bug = _context.machine.Single(x => x.machineID == model.machineID);
                bug.machineID = model.machineID;
                bug.description = model.description;
                bug.locationID = model.locationID;
                _context.Update(bug);
                _context.SaveChanges();
            }
            catch (System.Exception)
            {

                return false;
            }
            return true;

        }

        public bool Delete(string id)
        {
            try
            {
                var item = _context.machine.FirstOrDefault(x => x.machineID == id);
                _context.Remove(item);
                _context.SaveChanges();
            }
            catch (System.Exception)
            {

                return false;
            }
            return true;

        }
    }
}
