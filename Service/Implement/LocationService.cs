﻿using AutoMapper;
using Data;
using Data.Models;
using Data.ViewModel.Slide;
using Microsoft.EntityFrameworkCore;
using OfficeOpenXml;
using OfficeOpenXml.Table;
using Service.Helpers;
using Service.Interface;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Implement
{
    public class LocationService : ILocationService
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;

        //public static IWebHostEnvironment _environment;
        public LocationService(DataContext context , IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
            //_environment = environment;
        }

        //public async Task<bool> Create(Slide entity)
        //{
        //    await _context.Slides.AddAsync(entity);

        //    try
        //    {
        //        await _context.SaveChangesAsync();
        //        return true;
        //    }
        //    catch (Exception)
        //    {
        //        return false;

        //    }


        //}

        //public async Task<bool> Delete(int id)
        //{
        //    var entity = await _context.Slides.FindAsync(id);
        //    if (entity == null)
        //    {
        //        return false;
        //    }

        //     _context.Slides.Remove(entity);
        //    try
        //    {
        //        await _context.SaveChangesAsync();
        //        return true;
        //    }
        //    catch (Exception)
        //    {
        //        return false;

        //    }
        //}
        public Location Get(int id)
        {
            var result = new Location();
            try
            {
                result = _context.location.Single(x => x.id == id);
            }
            catch (System.Exception)
            {

            }
            return result;

        }

        public bool Add(Location model)
        {
            try
            {
                _context.Add(model);
                _context.SaveChanges();
            }
            catch (System.Exception)
            {

                return false;
            }
            return true;

        }

        public bool Update(Location model)
        {
            try
            {
                var bug = _context.location.Single(x => x.id == model.id);
                bug.locationname = model.locationname;

                _context.Update(bug);
                _context.SaveChanges();
            }
            catch (System.Exception)
            {

                return false;
            }
            return true;

        }

        public bool Delete(int id)
        {
            try
            {
                _context.Entry(new Location { id = id }).State = EntityState.Deleted; ;
                _context.SaveChanges();
            }
            catch (System.Exception)
            {

                return false;
            }
            return true;

        }
    }
}
