﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Data;
using Data.Models;
using Service.Interface;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using System.Net.Http.Headers;
using Microsoft.Extensions.Configuration;

namespace WorkManagement.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class RawController : ControllerBase
    {
        private readonly DataContext _context;
        private readonly IRawService _rawService;
        public readonly IWebHostEnvironment _environment;
        private readonly IConfiguration _configuaration;

        public RawController(DataContext context, IRawService rawService, IWebHostEnvironment environment, IConfiguration configuration)
        {
            _configuaration = configuration;
            _context = context;

            _rawService = rawService;
            _environment = environment;

        }

        // GET: api/Projects
    
        [HttpGet]
        public async Task<ActionResult> GetAll()
        {
            return Ok(await _rawService.GetAll());
        }

        [HttpGet("{id}/{start?}/{end?}")]
        public async Task<ActionResult> Detail(string id, string start = "", string end = "")
        {
            return Ok(  _rawService.GetDetail(id, start, end));
        }

        
        [HttpGet("{id}")]
        public async Task<ActionResult> Detail1(string id)
        {
            return Ok(_rawService.GetDetail1(id));
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetRPM(string id)
        {
            return Ok(await _rawService.GetRPM(id));
        }


        [HttpGet("{id}/{start?}/{end?}")]
        public IActionResult ExportExcel(string id, string start, string end)
        {
            byte[] data = _rawService.ExportExcel(id, start, end) as byte[];

            return File(data, "application/octet-stream", "DataUpload.xlsx");
        }

        [HttpGet("{id}")]
        public IActionResult GetChart(string id)
        {
            return Ok(_rawService.Test(id));
        }

        [HttpGet("{id}/{start?}/{end?}")]
        public IActionResult GetChart2(string id, string start = "", string end = "")
        {
            return Ok(_rawService.Test2(id, start, end));
        }
    }
}
