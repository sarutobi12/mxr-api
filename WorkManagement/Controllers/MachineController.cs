﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Data;
using Data.Models;
using Service.Interface;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using System.Net.Http.Headers;
using Microsoft.Extensions.Configuration;

namespace WorkManagement.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class MachineController : ControllerBase
    {
        private readonly DataContext _context;
        private readonly IMachineService _machineService;
        public readonly IWebHostEnvironment _environment;
        private readonly IConfiguration _configuaration;

        public MachineController(DataContext context, IMachineService machineService, IWebHostEnvironment environment, IConfiguration configuration)
        {
            _configuaration = configuration;
            _context = context;

            _machineService = machineService;
            _environment = environment;

        }

        // GET: api/Projects
        [HttpGet]
        public async Task<ActionResult> GetAll()
        {
            return Ok(await _machineService.GetAll());
        }

        [HttpGet("{id}")]
        public IActionResult Get(string id)
        {
            return Ok(_machineService.Get(id));
        }

        [HttpPost]
        public IActionResult Post([FromBody] Machine model)
        {
            return Ok(_machineService.Add(model));
        }

        [HttpPut]
        public IActionResult Put([FromBody] Machine model)
        {
            return Ok(_machineService.Update(model));
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public IActionResult Delete(string id)
        {
            return Ok(_machineService.Delete(id));
        }

    }
}
