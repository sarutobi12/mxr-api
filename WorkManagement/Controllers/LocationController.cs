﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Data;
using Data.Models;
using Service.Interface;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using System.Net.Http.Headers;
using Microsoft.Extensions.Configuration;

namespace WorkManagement.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class LocationController : ControllerBase
    {
        private readonly DataContext _context;
        private readonly ILocationService _locationService;
        public readonly IWebHostEnvironment _environment;
        private readonly IConfiguration _configuaration;

        public LocationController(DataContext context, ILocationService locationService, IWebHostEnvironment environment, IConfiguration configuration)
        {
            _configuaration = configuration;
            _context = context;
            _locationService = locationService;
            _environment = environment;

        }

        // GET: api/Projects
      
        // GET api/values/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            return Ok(_locationService.Get(id));
        }

        // POST api/values
        [HttpPost]
        public IActionResult Post([FromBody] Location model)
        {
            return Ok(_locationService.Add(model));
        }

        // PUT api/values/5
        [HttpPut]
        public IActionResult Put([FromBody] Location model)
        {
            return Ok(_locationService.Update(model));
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            return Ok(_locationService.Delete(id));
        }

    }
}
