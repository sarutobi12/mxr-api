﻿/*
  setting
  -------
  Exporting all rows
*/
INSERT INTO setting (id, standardRPM, minRPM, startBuzzerAfter, stopwatch, timer) VALUES ('EM002', 500, 250, 500, 10, 10);
INSERT INTO setting (id, standardRPM, minRPM, startBuzzerAfter, stopwatch, timer) VALUES ('EM001', 350, 250, 350, 10, 10);
/* 2 row(s) exported */

