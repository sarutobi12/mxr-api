﻿/*
  machine
  -------
  Exporting all rows
*/
INSERT INTO machine (id, machineID, description, locationID) VALUES (1, 'EM001', 'Water based', 1);
INSERT INTO machine (id, machineID, description, locationID) VALUES (2, 'EM002', 'Solvent based', 2);
/* 2 row(s) exported */

